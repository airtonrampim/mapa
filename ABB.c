#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "ABB.h"

/* Tipo exportado */

struct stc_Tree {
    Nodo *root;
    int nelem;
};

struct stc_Nodo {
    tipo_elem A;
    int isExt;
    struct stc_Nodo *esq, *dir, *pai;
};

/* Elemento vazio */
const tipo_elem empty_elem = {"",""};


/* Operacoes internas */

/* Remove todos os nodos da arvore T de raiz v
 * A funcao deve ser utilizada somente no destroyTree, pois ela nao atualiza os dados da arvore
 */
void freeAllNodes(Tree *T, Nodo *v);

/* Operacoes */
void defineTree(Tree **T) {
    /* Apenas cria uma nova instancia da arvore e inicializa os seus campos */
    *T = (Tree*) malloc(sizeof(Tree));
    (*T)->root = NULL;
    (*T)->nelem = 0;
}

void destroyTree(Tree **T) {
    /* Remove todos os elementos da arvore, remove a arvore e altera o campo T para NULL */
    if(*T != NULL) {
        if(!isEmpty(*T))
            freeAllNodes(*T, (*T)->root);
        free(*T);
        *T = NULL;
    }
}

int size(Tree *T) {
    return T->nelem;
}

int isEmpty(Tree *T) {
    return size(T) == 0;
}

Nodo *root(Tree *T) {
    return T->root;
}

Nodo *parent(Nodo *p) {
    return p->pai;
}

int isInternal(Nodo *p) {
    return !isExternal(p);
}

int isExternal(Nodo *p) {
    return p->isExt;
}

int isRoot(Tree *T, Nodo *p) {
    return p == root(T);
}

int isLeftChild(Tree *T, Nodo *p) {
    int result = 0;
    if(!isRoot(T,p))
        result = p == leftChild(parent(p));
    return result;
}

int isRightChild(Tree *T, Nodo *p) {
    int result = 0;
    if(!isRoot(T,p))
        result = p == rightChild(parent(p));
    return result;
}

void replaceTree(tipo_elem e, Nodo *p) {
    p->A = e;
}

Nodo *leftChild(Nodo *p) {
    return p->esq;
}

Nodo *rightChild(Nodo *p) {
    return p->dir;
}

Nodo *brother(Tree *T, Nodo *p) {
    Nodo *result = NULL;
    if(!isRoot(T,p))
        result = (p == leftChild(parent(p))) ? rightChild(parent(p)) : leftChild(parent(p));
    return result;
}

void expandExternal(Nodo *p) {
    /* Transforma o nodo externo p em interno com dois filhos nos externos com o elemento vazio definido */
    Nodo *esq, *dir;

    if(isExternal(p)) {
        esq = newNode(empty_elem, p);
        dir = newNode(empty_elem, p);
        
        p->esq = esq;
        p->dir = dir;
        
        p->isExt = 0;
    }
}

void removeAboveExternal(Tree *T, Nodo *p) {
    Nodo *x;

    if(isExternal(p)) {
        if(isRoot(T,parent(p))) {
            /* Remove o pai de p (raiz) e assume o seu irmao como raiz, caso este seja um no interno */
            free(root(T));
            /* Se o irmao de p tambem e um no externo, isto indica que a arvore tem apenas um unico elemento
               Logo, a funcao limpara a arvore */
            if(isExternal(brother(T,p))) {
                free(brother(T,p));
                T->root = NULL;
            /* Caso contrario, o irmao de p sera a nova raiz da arvore */
            } else
                T->root = brother(T,p);
        }
        else {
            x = brother(T,p);
            x->pai = p->pai->pai;
            /* Atualiza no avo de p a referencia para o novo pai (x) */
            if(isRightChild(T, parent(p))) {
                free(p->pai->pai->dir);
                p->pai->pai->dir = x;
            } else {
                free(p->pai->pai->esq);
                p->pai->pai->esq = x;
            }
        }
        free(p);
        T->nelem--;
    }
}

Nodo* encontreSubstituto(Nodo *p) {
    Nodo *pa;
    pa = rightChild(p);
    while(isInternal(pa))
        pa = leftChild(pa);
    return parent(pa);
}

Nodo* newNode(tipo_elem x, Nodo *pai) {
    Nodo *result = (Nodo*) malloc(sizeof(Nodo));
    result->A = x;
    result->isExt = 1;
    result->dir = result->esq = NULL;
    result->pai = pai;

    return result;
}

Nodo *searchAuxTree(chave k, Nodo *p) {
    tipo_elem x;

    /* Condicao de parada: Chegada ate os nos externos */
    if(isExternal(p))
        return p;
    else {
        x = p->A;
        if(strcmp(k, x.key) < 0)
            return searchAuxTree(k, leftChild(p));
        else if(strcmp(k, x.key) > 0)
            return searchAuxTree(k, rightChild(p));
        else
            return p;
    }
}

tipo_elem searchTreeByNode(chave k, Nodo *p) {
    Nodo *v;
    
    v = searchAuxTree(k,p);
    
    /* O elemento vazio (empty_elem) e o valor padrao para todo no externo da arvore */
    return v->A;
}

tipo_elem searchTree(Tree *T, chave k) {
    tipo_elem result = empty_elem;

    if(!isEmpty(T))
        result = searchTreeByNode(k, root(T));
    
    return result;
}

int insertTree(Tree *T, tipo_elem x) {
    Nodo *v;
    int result = 1;

    /* Se a lista estiver vazia (root = NULL) criar uma nova raiz */
    if (isEmpty(T)) {
        T->root = newNode(x, NULL);
        expandExternal(T->root);
        T->nelem++;
    /* Encontra o no externo da arvore onde o novo elemento x deve ser inserido e o transforma em no interno
       Caso o elemento exista, a funcao searchAuxTree retornara uma referencia para um no interno e o processo nao sera executado */
    } else {
        v = searchAuxTree(x.key, root(T));
        result = isExternal(v);

        if(result) {
            expandExternal(v);
            replaceTree(x,v);
            T->nelem++;
        }
    }

    return result;
}

tipo_elem removeTree(Tree *T, chave k) {
    /* v: Nodo com a chave k
       z: Nodo externo a ser removido
       w: Nodo com o elemento candidato a ser substituido
       x: Elemento de v */
    Nodo *v, *z, *w;
    tipo_elem x = empty_elem;

    if(!isEmpty(T)) {
        v = searchAuxTree(k,root(T));
        x = v->A;
        if(isInternal(v)) {
            if(isExternal(leftChild(v)))
                z = leftChild(v);
            else if(isExternal(rightChild(v)))
                z = rightChild(v);
            else {
                w = encontreSubstituto(v);
                replaceTree(w->A,v);
                /* A funcao encontreSubstituto retornara o elemento mais profundo da descendencia direita da arvore
                   Desta forma, o filho do no retornado sempre sera um no externo */
                z = leftChild(w);
            }
            removeAboveExternal(T,z);
        }
    }

    return x;
}

void visite(Nodo *p) {
    printf("%s\t%s\n", p->A.key, p->A.info);
}

void interfixTreeByNode(Nodo *p) {
    if(isInternal(p)) {
        interfixTreeByNode(leftChild(p));
        visite(p);
        interfixTreeByNode(rightChild(p));
    }
}
void interfixTree (Tree *T) {
    if(!isEmpty(T))
        interfixTreeByNode(root(T));
}

/* Operacoes internas - implementacao */

void freeAllNodes(Tree *T, Nodo *v) {
    if(isInternal(v)) {
        freeAllNodes(T, leftChild(v));
        freeAllNodes(T, rightChild(v));
    }
    free(v);
}