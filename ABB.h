/* TAD Arvore Binaria Dinamica */

/* Tipo exportado */
typedef char chave[31];

typedef struct {
    chave key;
    chave info;
} tipo_elem;

typedef struct stc_Nodo Nodo;
typedef struct stc_Tree Tree;

/* Constroi a arvore
 */ 
void defineTree(Tree **T);

/* Destroi a arvore
 */
void destroyTree(Tree **T);

/* Retorna a quantidade de elementos da arvore
 */
int size(Tree *T);

/* Verifica se a arvore possui algum elemento
 */
int isEmpty(Tree *T);

/* Retorna uma referencia para a raiz da arvore
 */
Nodo *root(Tree *T);

/* Retorna uma referencia para o pai do no p
 */
Nodo *parent(Nodo *p);

/* Verifica se o no p e um no interno
 */
int isInternal(Nodo *p);

/* Verifica se o no p e um no externo
 */
int isExternal(Nodo *p);

/* Verifica se o no p e a raiz da arvore T
 */
int isRoot(Tree *T, Nodo *p);

/* Verifica se o no p e o filho esquerdo do seu pai
 * Caso p seja a raiz, sera retornado 0
 */
int isLeftChild(Tree *T, Nodo *p);

/* Verifica se o no p e o filho direito do seu pai
 * Caso p seja a raiz, sera retornado 0
 */
int isRightChild(Tree *T, Nodo *p);

/* Substitui o elemento do nodo p por e 
 */
void replaceTree(tipo_elem e, Nodo *p);

/* Retorna o filho esquerdo de p 
 */
Nodo *leftChild(Nodo *p);

/* Retorna o filho direito de p
 */
Nodo *rightChild(Nodo *p);

/* Retorna o irmao de p
 * Caso p seja a raiz, sera retornado NULL
 */
Nodo *brother(Tree *T, Nodo *p);

/* Transforma p em um no interno criando dois nos externos como filhos de p
 * Caso p seja interno, nenhum processo sera executado 
 */
void expandExternal(Nodo *p);

/* Remove o pai de p, o proprio p e define no lugar do pai o irmao de p
 * Caso o pai de p seja a raiz e o irmao de p um no externo, a arvore sera limpa
 */
void removeAboveExternal(Tree *T, Nodo *p);

/* Encontra o nodo com o elemento de menor chave na descendencia direita do nodo p
 */
Nodo *encontreSubstituto(Nodo *p);

/* Cria um novo no externo para o elemento x com o pai especificado
 */
Nodo* newNode(tipo_elem x, Nodo *pai);

/* Retorna o no contendo a chave k na subarvore de raiz p em T
 * Caso a chave nao seja encontrada, sera retornada uma referencia para o no externo da subarvore 
 * onde o elemento de chave k pode ser inserido sem violar a estrutura da ABB
 */
Nodo *searchAuxTree(chave k, Nodo *p);

/* Retorna o elemento de chave k da arvore
 * Caso a chave nao seja encontrada, sera retornado um elemento vazio (key = "", info = "")
 */
tipo_elem searchTree(Tree *T, chave k);

/* Retorna o elemento de chave k na subarvore de raiz p em T
 * Caso a chave nao seja encontrada, sera retornado um elemento vazio (key = "", info = "")
 */
tipo_elem searchTreeByNode(chave k, Nodo *p);

/* Insere na arvore T o elemento x
 * Caso ja exista um elemento de mesma chave na arvore, a insercao nao sera realizada
 * O retorno indica se a insercao foi realizada
 */
int insertTree(Tree *T, tipo_elem x);

/* Remove na arvore T o elemento de chave k
 * Caso o elemento nao seja encontrado, a remocao nao sera realizada
 * O retorno indica se a remocao foi realizada
 */
tipo_elem removeTree(Tree *T, chave k);

/* Imprime em console os dados do elemento de p
 */
void visite(Nodo *p);

/* Efetua um percurso interfixado na subarvore de raiz p
 */
void interfixTreeByNode(Nodo *p);

/* Efetua um percurso interfixado na arvore T
 */
void interfixTree(Tree *T);