
#include <stdio.h>
#include <stdlib.h>

#include "Mapa.h"

struct stc_Mapa {
    Tree *tree;
};

/* Operadores */

void defineMap(Mapa **M) {
    *M = (Mapa*) malloc(sizeof(Mapa));
    defineTree(&(*M)->tree);
}

void destroyMap(Mapa **M) {
    destroyTree(&(*M)->tree);
    free(*M);
    *M = NULL;
}

tipo_elem searchMap(Mapa *M, chave k) {
    return searchTree(M->tree, k);
}

int insertMap(Mapa *M, tipo_elem x) {
    return insertTree(M->tree, x);
}

int replaceMap(Mapa *M, tipo_elem x) {
    Nodo *v;
    int result = 0;

    if(!isEmpty(M->tree))
    {
        v = searchAuxTree(x.key, root(M->tree));
        result = isInternal(v);

        if(result)
            replaceTree(x,v);
    }

    return result;
}

tipo_elem removeMap(Mapa *M, chave k) {
    return removeTree(M->tree, k);
}

void listMap(Mapa *M) {
    interfixTree(M->tree);
}