
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Mapa.h"

#define OPCS_MENU "\
Informe uma das seguintes opcoes:\n\
\n\
1) Inserir elemento\n\
2) Remover elemento\n\
3) Substituir elemento\n\
4) Procurar elemento\n\
5) Listar elementos\n\
0) Sair\n"

void readConsole(char *format, void *output);
void writeConsole();
tipo_elem readElem();
void writeElem(tipo_elem x);

void inserir(Mapa *M);
void remover(Mapa *M);
void substituir(Mapa *M);
void procurar(Mapa *M);
void listar(Mapa *M);

int main(int argc, char** argv) {
    char opc;
    Mapa *M;

    defineMap(&M);

    writeConsole(OPCS_MENU);
    readConsole(" %c", &opc);
    while (opc != '0') {
        switch(opc) {
            case '1': inserir(M);
                break;
            case '2': remover(M);
                break;
            case '3': substituir(M);
                break;
            case '4': procurar(M);
                break;
            case '5': listar(M);
                break;
            default: writeConsole("Opcao invalida");
        }
        writeConsole(OPCS_MENU);
        readConsole(" %c",&opc);
    }

    destroyMap(&M);

    return 0;
}

void readConsole(char *format, void* output) {
    fputs(": ", stdout);
    scanf(format, output);
}

void writeConsole(char *str) {
    printf("%s\n", str);
}

tipo_elem readElem() {
    tipo_elem result;

    writeConsole("Informe a chave");
    readConsole(" %[^\n]", result.key);
    writeConsole("Informe o valor");
    readConsole(" %[^\n]", result.info);

    return result;
}

void writeElem(tipo_elem x) {
    printf("Chave: %s\nInfo: %s\n", x.key, x.info);
}

void inserir(Mapa *M) {
    tipo_elem x;

    x = readElem();
    if(insertMap(M, x))
        writeConsole("Insercao realizada com sucesso");
    else
        writeConsole("Insercao nao realizada. Ja existe outro elemento com a mesma chave");
}

void remover(Mapa *M) {
    tipo_elem x;
    writeConsole("Insira a chave do elemento a ser removido");
    readConsole(" %[^\n]", x.key);
    
    x = removeMap(M, x.key);
    if(strcmp(x.key,"") == 0)
        writeConsole("Elemento nao encontrado");
    else {
        writeConsole("Elemento removido com sucesso. Dados do elemento removido:");
        writeElem(x);
    }
}

void substituir(Mapa *M) {
    tipo_elem x;
    writeConsole("Insira o elemento a ser substituido");

    x = readElem();
    if(replaceMap(M, x))
        writeConsole("Elemento substituido com sucesso");
    else
        writeConsole("Elemento nao encontrado");
}

void procurar(Mapa *M) {
    tipo_elem x;
    writeConsole("Insira a chave do elemento");
    readConsole(" %[^\n]", x.key);

    x = searchMap(M, x.key);
    if(strcmp(x.key,"") == 0)
        writeConsole("Elemento nao encontrado");
    else {
        writeElem(x);
    }
}

void listar(Mapa *M) {
    listMap(M);
}