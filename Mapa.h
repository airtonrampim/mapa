
#include "ABB.h"

typedef struct stc_Mapa Mapa; 

/* Constroi o mapa
*/
void defineMap(Mapa **M);

/* Destroi o mapa
*/
void destroyMap(Mapa **M);

/* Retorna o elemento com chave k do mapa M, caso exista. Caso contrario, 
retorna elemento nulo 
*/
tipo_elem searchMap(Mapa *M, chave k);

/* Insere o elemento x no mapa M e retorna 1, caso nao exista elemento em M com
 a mesma chave de x. Se ja existir elemento em M com a mesma chave de x, retorna
 0
*/
int insertMap(Mapa *M, tipo_elem x);

/* Substitui por x o elemento do mapa M com a mesma chave de x, se tal elemento
 existe em M, retornando 1. Caso contrario retorna 0
*/
int replaceMap(Mapa *M, tipo_elem x);

/* Remove e retorna o elemento com chave k, caso este exista no mapa M. Caso 
contrario retorna elemento nulo
*/
tipo_elem removeMap(Mapa *M, chave k);

/* Lista todos os elementos do mapa M
 */ 
void listMap(Mapa *M);