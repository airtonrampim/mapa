CC = gcc
CFLAGS = -Wall -ansi
MAIN = Mapa

all: $(MAIN)

$(MAIN): ABB.o Mapa.o Main.o
	$(CC) $(CFLAGS) -o $(MAIN) ABB.o Mapa.o Main.o

ABB.o: ABB.c ABB.h
	$(CC) $(CFLAGS) -c ABB.c

Mapa.o: Mapa.c Mapa.h ABB.h
	$(CC) $(CFLAGS) -c Mapa.c

Main.o: Main.c Mapa.h
	$(CC) $(CFLAGS) -c Main.c

clean:
	$(RM) *.o *~ $(MAIN)
